# Phương pháp tính checksum

(Các) tải trọng dữ liệu trong mỗi yêu cầu gửi lên thường ở định dạng dữ liệu có cấu trúc (`JSON`,`XML`,**`sequential`**,...).
Để đảm bảo an toàn và tính toàn vẹn của các dữ liệu, tải trọng dữ liệu gửi lên máy chủ của Sơn Phát, cần bao gồm các thuộc tính bắt buộc và chuộc tính `Checksum` nhằm xác nhận lại các thuộc tính dữ liệu theo yêu cầu.

Nguyên tắc:

**Trong mỗi đơn vị tải trọng, GIÁ TRỊ của các thuộc tính được đánh dấu sẽ được sắp xếp tuần tự theo đúng thứ tự đã quy định trong cấu trúc tải trọng.
Phân tách của các giá trị trên, thông thường sẽ sử dụng ký tự `|`, nếu sử dụng ký tự khác sẽ được đề cập cụ thể riêng.**

**Chuỗi tuần tự trên sau đó sẽ được nối thêm `Mã bảo mật` ở đầu hoặc cuối chuỗi, cuối cùng sử dụng hàm băm mật mã SHA256 để tạo ra mã xác nhận cuối cùng (`Checksum`).**

Giả định tải trọng dữ liệu có nội dung sư nhau:
```BASH
/** URL Query encoded */
?MaBenXe=bc827e38-c7a4-4854-a9cc-182d8d5ee781
&BienKiemSoat=20A23401
&MaTuyen=2092.1111.A
```
```JSON
/** JSON */
{
    "MaBenXe": "bc827e38-c7a4-4854-a9cc-182d8d5ee781",
    "BienKiemSoat": "20A23401",
    "MaTuyen": "2092.1111.A"
}
```
```XML
/** XML */
<Payload>
    <MaBenXe>bc827e38-c7a4-4854-a9cc-182d8d5ee781</MaBenXe>
    <BienKiemSoat>20A23401</BienKiemSoat>
    <MaTuyen>2092.1111.A</MaTuyen>
</Payload>
```
Theo đó, thứ tự thành phần được xác định là `<MaBenXe>|<BienKiemSoat>|<MaTuyen>`, do vậy tính toán ra chuỗi tuần tự sẽ có kết quả là
`bc827e38-c7a4-4854-a9cc-182d8d5ee781|20A23401|2092.1111.A`. Đặc tả API yêu cầu đặt mã bảo mật vào đầu chuỗi tuần tự dữ liệu, do vậy giá trị `Checksum` sẽ được tính là:
```PHP
hash("sha256", $MaBaoMat . "bc827e38-c7a4-4854-a9cc-182d8d5ee781|20A23401|2092.1111.A");
```

Mã mẫu được biểu diễn:

- PHP
```PHP
<?php
function checksum ($secret, $values, $glue = "|", $secretKeyAtEnd = false) {
    $sequentialValuesInTxt = implode($glue, $values);
    if (!$secretKeyAtEnd) {
        $sequentialValuesInTxt = $secret . $glue . $sequentialValuesInTxt;
    } else {
        $sequentialValuesInTxt .= $glue . $secret;
    }
    return hash("sha256", $sequentialValuesInTxt);
}

$secret = "M@ba0MAT-doSonphaTcungC4P";

$payload = [
    "MaBenXe" => "bc827e38-c7a4-4854-a9cc-182d8d5ee781",
    "BienKiemSoat" => "20A-23401",
    "MaSoThueDoanhNghiep" => "4601328480",
];

$payloadCheksum = checksum($secret, [
    $payload["MaBenXe"],
    $payload["BienKiemSoat"],
    $payload["MaSoThueDoanhNghiep"],
], "", false);

$payload["Checksum"] = $payloadCheksum;

return $payload;
```

- C#
```cs
using System.Security.Cryptography;
using System.Text;

byte[] checksum (string secret, string[] args, bool secretAtEnd = false, string separator = "|") {
    if (secretAtEnd) {
        args = args.Append(secret).ToArray();
    } else {
        args = args.Prepend(secret).ToArray();
    }
    
    string text = string.Join(separator, args);
    
    byte[] buffer = new byte[32];
    if (SHA256.TryHashData(Encoding.UTF8.GetBytes(text), buffer, out _ )) {
        return buffer;
    } else {
        throw new CryptographicException("Your computer cannot handle SHA256.");
    }
}

var payload = new {
    MaBenXe = "bc827e38-c7a4-4854-a9cc-182d8d5ee781",
    BienKiemSoat = "20A-23401",
    MaSoThueDoanhNghiep = "4601328480",
};

var payloadChecksumRaw = checksum("Fymv3jNW)s82#w2^Hee!@DI3)Ym%dW(XaK798*X23Ib!qy)6Zu", separator: "", args: new string[]{
    payload.MaBenXe,
    payload.BienKiemSoat,
    payload.MaSoThueDoanhNghiep,
});
var payloadChecksum = BitConverter.ToString(payloadChecksumRaw).Replace("-", "").ToLower();

var finalPayload = new {
    payload.MaBenXe, payload.BienKiemSoat, payload.MaSoThueDoanhNghiep,
    Checksum = payloadChecksum
};

Console.WriteLine("Payload:");
Console.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(finalPayload, Newtonsoft.Json.Formatting.Indented));
```